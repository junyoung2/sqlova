### Preliminaries
- Make and activate conda environment using `clove_env.yml` in `environments` directory
- Download and unzip `bert_and_wikisql.zip` in `sqlova` directory (the main directory where run_train.sh is)
    - [Download link](https://drive.google.com/file/d/1UCDN5cdMksKYp6hdIlPLABahB_4fAn7F/view?usp=sharing)

### How to run it
- Make directory called `data`
- In the data directory, make a directory with the dataset name you want to train with (we'll call it DATASET_DIR)
- In DATASET_DIR, make directory with seed number as name (we'll call it SEED)
- In `data/DATASET_DIR/SEED`, put in 'train_tok.jsonl', the training dataset you want to train with
- Run `sh run_train.sh GPU SEED data/DATASET_DIR` to start training
    - where GPU is gpu # to use, SEED is what number seed you want to experiment with
    - Ex) Download wikisql.zip and unzip in directory called `data` [Download link](https://drive.google.com/file/d/1E7xs4QgrQ_j2btjOYGxzoxMEB7WiEVo5/view?usp=sharing0)
    - `sh run_train.sh 0 1 data/wikisql`
- In `data/DATASET_DIR/SEED`, the models with the best performance on the WikiSQL dev set will be saved.

### Evaluation
- Assuiming you trained on a directory called `data/DATASET_DIR/SEED`,
- Run `sh predict.sh GPU data/DATASET_DIR/SEED`
    - where GPU is gpu # to use
- in `data/DATASET_DIR/SEED`, prediction results on the WikiSQL dev set will be saved in a file called results_dev.jsonl
- Run `python evaluate_ws.py --pred_file=data/DATASET_DIR/SEED/results_dev.jsonl` to see accuracy on WikiSQL dev set
